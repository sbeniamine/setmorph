![pipeline](https://gitlab.com/sbeniamine/setmorph/badges/master/pipeline.svg?job=test)


# setMorph

Calculate distribution of segmented formatives from their distributions.

If you use this software, please cite:

-  Sacha Beniamine, Mae Carroll (2023). [The other perspective on exponence](https://ismo2023.ovh/fichiers/abstracts/15_ISMO_2023_Beniamine_caroll.pdf). Presentation at International Symposium of Morphology 2023, Nancy, Sep 15, 2023 
- Beniamine, Sacha and Guzmán Naranjo, Matías (2021) "[Multiple alignments of inflectional paradigms](https://scholarworks.umass.edu/scil/vol4/iss1/21)" *Proceedings of the Society for Computation in Linguistics*: Vol. 4 , Article 21,


# Install 

Install this repository:

~~~
pip install 'setmorph @ git+https://git@gitlab.com/sbeniamine/setmorph.git'
~~~

# Run this script

This script can be run as:

~~~ bash
setmorph <forms.csv> <features.csv> <output_path>
~~~


# Full run from a Paralex lexicon

- Sounds file: see [Inventory](http://www.gitlab.com/sbeniamine/inventory/) and [Paralex standard](https://www.paralex-standard.org)
- Paradigms and features files: see  [Paralex standard](https://www.paralex-standard.org)

## Installing everything


Use catconfig to install CLTS data:

~~~
pip install cldfbench pyglottolog pyclts
cldfbench catconfig
~~~

You only need to clone `CLTS`, but not concepticon or glottolog.

Install [morphalign](http://www.gitlab.com/sbeniamine/morphalign/), [segmentations](http://www.gitlab.com/sbeniamine/segmentations), and [setmorph](http://www.gitlab.com/sbeniamine/setmorph/):

~~~
pip install 'morphalign @ git+https://git@gitlab.com/sbeniamine/morphalign.git'
pip install 'segmentations @ git+https://git@gitlab.com/sbeniamine/segmentations.git'
pip install 'setmorph @ git+https://git@gitlab.com/sbeniamine/setmorph.git'
~~~


## Alignment

First, align the paradigms using [morphalign](http://www.gitlab.com/sbeniamine/morphalign/):

~~~
morphalign -f <sounds_file> -cpu 7 -s features -o aligned_paradigms.csv -- paradigms.csv
~~~

- Alternatively, use `-s frisch` to base sound similarity on shared sets of natural classes; `-a features` to base it on shared features; `-a simple` to use simple edit distances biased to defavor any non-identical alignments (this method does not use a sound file for alignments).
- Another option is to specify `-f bipa` instead of a sound file.

## Segmentation 

This produces segmented formatives from aligned paradigms, using [Segmentations](http://www.gitlab.com/sbeniamine/segmentation/)


~~~
segment -t -o output_file.csv -f <sounds_file>  -- aligned_paradigms.csv 
~~~

Here, it is again possible to specify `-f bipa` instead of a sound file.

## Distributions 

This computes the meaning of each formative.

Run as:

~~~ bash
setmorph <forms.csv> <features.csv> <output_path>
~~~

- `forms.csv` is the output of the [segmentation]() package.
- `features.csv` specifies the morphological feature space, and is a [Paralex](https://paralex-standard.org/specs/#features-values) features values table.
- `output_path` indicates where to create the resulting file.
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from .setmorph import read_paradigms, read_features, get_exponents
import argparse

def format_cell(vals):
    return ".".join(vals)

def format_feature_values(vals):
    return " ".join(format_cell(v) for v in vals)

def analyse_exponence(forms_path, features_path, output_prefix):
    df = read_paradigms(forms_path)
    fs = read_features(features_path)
    print("Computing form<->meaning mapping...")
    exponents = get_exponents(df, fs)

    for col in ["exponence", "dist"]:
        exponents[col] = exponents[col].apply(format_feature_values)

    exponents.to_csv(output_prefix + "_formatives.csv", index=False)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("forms", type=str, help="Forms table")
    parser.add_argument("features", type=str, help="Features table")
    parser.add_argument("output", type=str, help="Output path and prefix")
    args = parser.parse_args()
    analyse_exponence(args.forms, args.features, args.output)

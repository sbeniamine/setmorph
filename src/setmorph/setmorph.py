import logging
import pandas as pd
from itertools import combinations, chain, product
from collections import Counter
from typing import NamedTuple
from tqdm import tqdm
tqdm.pandas()

def check_cell_structure(cells):
    """ Checks that the cells are not malformed.

    - Cells must not be subsets of other cells.

    Args:
        cell_series: a set of cells

    Returns:
        None if everything is alright, otherwise throws an exception.

    """
    incl = []
    cells = sorted(cells, key=len)
    for i, c1 in enumerate(cells):
        for c2 in cells[i + 1:]:
            if c1 < c2:
                incl.append(".".join(c1) + " < " + ".".join(c2))
    if incl:
        msg = "Malformed paradigm structure: " \
              "cells can not be subsets of other cells. " \
              "I found:\n" + "\n".join(incl)
        raise ValueError(msg)


def read_features(path):
    """ Reads a features file.

    Args:
        path (str): path to a table of features.

    Returns:
        A dict of features to sets of fozensets containing each a single  value.
        values are given as frozensets to facilitate set operations.
    """
    df = pd.read_csv(path)
    df["value_id"] = df["value_id"].apply(lambda x: frozenset({x}))
    features_to_values = df.groupby("feature").agg(set).to_dict()["value_id"]
    for f, vs in features_to_values.items():
        if len(vs) == 1:
            v = "".join(list(vs)[0])
            logging.warning(f"The feature `{f}` has a single value `{v}`. "
                            f"No exponents of `{v}` will be possible.")
    return features_to_values


def read_paradigms(path):
    """ Reads a paradigms file.

    Args:
        path (str): path to a table of segmented formatives.

    Returns:
        a pd.Dataframe of segmented formatives. Cells are parsed into lists of frozensets.
    """
    checked = set()
    def check_lexeme_cells(group):
        cells = frozenset(group.cell.unique())
        if cells not in checked:
            checked.add(cells)
            check_cell_structure(cells)
    df = pd.read_csv(path)
    df.loc[:, 'cell'] = df.cell.str.split(".").apply(frozenset)
    df.groupby("lexeme").apply(check_lexeme_cells)
    return df


def exponence(cells, dista, feature_structure):
    """ Calculates the set of values a formative is the exponent of, from its distribution.

    This is calculated based on the formative's distribution. The set of exponential values
    is the set of descriptions which are in all of the deltas. Deltas are alternate ways of
    writing the distribution.

    Args:
        cells (set): the set of cells in which a lexeme occurs
        dista (set): the set of cells in which the formative occurs
        feature_structure (dict): A mapping of feature names (eg. "tense")
            to sets of values (eg. {"pst", "prs", "fut"}

    Returns:
        delta (set): a set of frozensets of values, representing exponential values.
    """
    if dista == cells:
        return set()

    # We check whether each cell in dista can be written more economically
    # Using subsets of its features
    freqs = Counter(chain(*cells))
    delta = set()
    spans = set()

    dista_ordered = sorted(dista,
                           key=lambda c: min([freqs[f] for f in c]),
                           reverse=True)
    for cell in dista_ordered:
        subsets = set()
        found = False
        length = len(cell)
        i = 1

        # We explore subsets of the cell of increasing length
        # For the subset relation check to work, we need to
        #  look first at features present in more cells
        while not found and i < length:
            combos = combinations(cell, i)
            for s in sorted(combos,
                            key=freqs.__getitem__,
                            reverse=True):
                # print(f"is {s} a description ?")
                s = frozenset(s)
                if s in delta | subsets:
                    # print(f"\t already in subsets or delta")
                    found = True
                else:
                    span_word = set(filter(lambda c: s <= c, cells))
                    span_dista = {c for c in dista if s <= c}
                    is_subset = any(span_word <= s for s in spans)
                    # print(f"\t span word: {span_word}")
                    # print(f"\t span dista: {span_dista}")
                    if span_word == span_dista and not is_subset:
                        subsets.add(s)
                        found = True
                        spans.add(frozenset(span_dista))
            i += 1
        if found:
            delta |= subsets
        else:
            delta.add(cell)

    # Check and remove dimensions which are fully filled
    for f, vs in feature_structure.items():
        if vs <= delta:
            delta -= vs
    return delta

def get_exponents(df, features):
    """ Returns all the exponence descriptions for an entire lexicon

    Args:
        df: a lexicon

    Returns:
        a pd.DataFrame associating each quadruple of (lexeme, tier, slot, formative)
            to a set of fv combinations it is an exponent of,
             the number of cells it occurs in,
            and its full distribution.
    """

    def exponence_word(paradigm):
        cells = set(paradigm.cell)
        f_values = set(chain(*cells))
        features_w = {f: {v for v in features[f] if v <= f_values}
                      for f in features}
        groups = paradigm.groupby(["tier", "slot", "formative"])
        transforms = {"cell": [frozenset, lambda d: exponence(cells, set(d), features_w)],
                      "full_slot": [lambda x: set(tuple(x))]}
        res = groups.agg(transforms)
        res.columns = ["dist", "exponence", "full_slot"]
        return res

    def merge_same_dist(exps):
        first = exps.iloc[0,:]
        if exps.shape[0] == 1:
            return first
        chars = set(chain(*exps["formative"]))
        first["formative"] = "".join(c for c in first["full_slot"].pop() if c in chars)
        first["tier"] = "/".join(exps.tier.sort_values())
        return first
    result = df.groupby("lexeme").progress_apply(exponence_word).reset_index()
    result = result.groupby(["lexeme", "slot", "dist"]).apply(merge_same_dist)
    result.drop("full_slot", inplace=True, axis=1)
    return result